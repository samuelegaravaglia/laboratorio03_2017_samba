package it.unimi.di.sweng.lab03;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class LinkedListTest {

	@Rule
    public Timeout globalTimeout = Timeout.seconds(2);

	private IntegerList list;
	
	/* @Test
	public void firstTestToBeReplaced() {
		fail("Not yet implemented.");
	}
	*/
	
	@Test
	public void costruttoreSenzaParam(){
		list=new IntegerList();
		assertThat(list.toString()).isEqualTo("[]");
	}
	
	@Test
	public void conParametri(){
		list=new IntegerList();
		list.addLast(1);
		assertThat(list.toString()).isEqualTo("[1]");
		list.addLast(3);
		assertThat(list.toString()).isEqualTo("[1 3]");

	}
	
	@Test
	public void constringa(){
		list=new IntegerList("");
		assertThat(list.toString()).isEqualTo("[]");
		list=new IntegerList("1");
		assertThat(list.toString()).isEqualTo("[1]");
		list=new IntegerList("1 2 3");
		assertThat(list.toString()).isEqualTo("[1 2 3]");
	}
	
	@Test
	public void illegalArgumentTest3(){
		try{
			list=new IntegerList("1 2 aaaa");
			failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
		}catch(IllegalArgumentException e){
			}
	}
	
	@Test
	public void aggiungiTesta(){
		list=new IntegerList();
		list.addFirst(1);
		assertThat(list.toString()).isEqualTo("[1]");
		list.addFirst(3);
		assertThat(list.toString()).isEqualTo("[3 1]");
	}
	
	@Test
	public void rimuoviTesta(){
		list=new IntegerList("1 2");
		assertThat(list.removeFirst()).isEqualTo(true);
		assertThat(list.toString()).isEqualTo("[2]");
		assertThat(list.removeFirst()).isEqualTo(true);
		assertThat(list.toString()).isEqualTo("[]");
		assertThat(list.removeFirst()).isEqualTo(false);
	}
	
	@Test
	public void rimuoviUno(){
		list=new IntegerList("1 2 3 4 3 5");
		assertThat(list.remove(2)).isEqualTo(true);
		assertThat(list.toString()).isEqualTo("[1 3 4 3 5]");
		assertThat(list.remove(3)).isEqualTo(true);
		assertThat(list.toString()).isEqualTo("[1 4 3 5]");
		assertThat(list.remove(6)).isEqualTo(false);
	}
	
	@Test
	public void rimuoviTutte(){
		list=new IntegerList("1 2 3 4 3 5");
		assertThat(list.removeAll(3)).isEqualTo(true);
		assertThat(list.toString()).isEqualTo("[1 2 4 5]");
		assertThat(list.removeAll(6)).isEqualTo(false);
	}
	
	@Test
	public void media(){
		list=new IntegerList("1 2");
		assertThat(list.mean()).isEqualTo(1.5);
		list=new IntegerList("160 591 114 229 230 270 128 1657 624 1503");
		assertThat(list.mean()).isEqualTo(550.6);
	}
	
	@Test
	public void primaDopo(){
		list=new IntegerList("1 2 3");
		assertThat(list.prev()).isEqualTo(1);
		assertThat(list.next()).isEqualTo(1);
		assertThat(list.prev()).isEqualTo(2);
		assertThat(list.next()).isEqualTo(1);
		assertThat(list.next()).isEqualTo(2);
		assertThat(list.next()).isEqualTo(3);


	}
	
}






















