package it.unimi.di.sweng.lab03;
import java.util.*;
import java.util.function.IntPredicate;
public class IntegerList {
	
	private Nodo testa;
	private Nodo ultimo;
	private Nodo visita;
	public IntegerList(){
	}
	
	public IntegerList(String s){
		if(s.equals("")){
			testa=ultimo=null;
		}
		else{	
			String[] vet=s.split(" ");
			for(int i=0;i<vet.length;i++)
				addLast(Integer.parseInt(vet[i]));
		}
	}
	
	
	@Override
 	public String toString(){
		StringBuilder result = new StringBuilder("[");
		Nodo currentNode =testa;
		int i=0;
		while (currentNode != null){
			if (i++ >0){
				result.append(' ');
			}
			result.append(currentNode.getValue());
			currentNode = currentNode.next();
		}
		return result + "]";
 	}

	
 	public void addLast(int value) {
		if (testa == null){
			testa =visita= ultimo = new Nodo(value);
		}
		else {
			Nodo node = new Nodo(value);
			ultimo.setNext(node);
			ultimo = node;
		}

 	}

	public void addFirst(int value) {
		if(testa==null)
			testa=ultimo=new Nodo(value);
		else{
			Nodo node=new Nodo(value);
			node.setNext(testa);
			testa=node;
		}
	}

	public boolean removeFirst() {
		boolean esito;
		if(testa==null && ultimo==null)
			esito=false;
		else{
			if(testa.next()==null){
				testa=ultimo=null;
				esito=true;}
			else{
				testa=testa.next();
				esito=true;
			}
		}
		return esito;
	}

	public boolean remove(int i) {
		boolean esito=true;
		Nodo n=testa;
		Nodo prev=null;
		do{
			prev=n;n=n.next();
		}while(n!=null && n.getValue()!=i);
		if(n==null)
			 esito=false;
		
		else if(n.getValue()==i && n!=ultimo){
				esito=true;
				prev.setNext(n.next());
				n.setNext(null);
			}
			else	if(n.getValue()==i && n==ultimo){
					esito=true;
					prev.setNext(null);
					ultimo=prev;
					n.setNext(null);
				}
		
		return esito;
	}//fine remove

	public boolean removeAll(int i) {
		boolean esito=false;int c=0;
		Nodo n=testa;
		Nodo prev=null;
		do{
			prev=n;n=n.next();
			if(n!=null && n.getValue()==i && n!=ultimo){
				prev.setNext(n.next());
				n.setNext(null);
				c++;}
			else if(n!=null && n.getValue()==i && n==ultimo){
					c++;
					prev.setNext(null);
					ultimo=prev;
					n.setNext(null);
				}
			n=prev.next();
		}while(n!=null);
		if(c>0)
			esito=true;
		return esito;
	}

	public double mean() {
		double media=0.0; int conta=0;
		Nodo n=testa;
		do{
			conta++;
			media+=(double)n.getValue();
			n=n.next();
		}while(n!=null);
		return media/conta;
	}

	public Integer next() {
		Integer n=setInt();
		if(visita!=ultimo){
			visita=visita.next();
		}	
		return n;
	}

	public Integer prev() {
		Integer n=setInt();
		if(visita!=testa){
			Nodo prec=testa;
			while(prec.next()!=visita){
				prec=prec.next();
			}
			visita=prec;
		}
		return n;
	}
	public Integer setInt(){
		return visita.getValue();
	}
}










